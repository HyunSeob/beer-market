# Beer Market

> NOTE: 이 레포지토리는 과제 제출을 위해서 만들어졌습니다.

과제의 빠른 확인을 위해서는 아래의 데모 링크를 참고하시면 됩니다.

[Demo](https://beer-market.now.sh)

혹은 아래의 가이드를 참고하셔서 직접 로컬에서 앱을 빌드하셔도 됩니다.

## 시작하기

이 프로젝트는 nvm & Node.js 기반으로 이루어져 있습니다. 따라서 아래의 절차를 따르기 전에 nvm이 설치되어 있는지 확인해주세요. 사용하고 있는 Node 버전은 Carbon(v8.x.x/LTS)입니다. nvm이 설치되어 있다면 Node 버전을 Carbon으로 맞춰주세요.

다음으로 이 레포지토리를 클론한 뒤에 레포지토리의 경로로가서 아래의 명령어를 사용하여 Node 버전을 맞춥니다.

```sh
$ git clone https://HyunSeob@bitbucket.org/HyunSeob/beer-market.git
$ cd beer-market
$ nvm use
```

이 프로젝트는 yarn을 기반으로 하고 있습니다. npm을 사용하셔도 큰 문제는 없을 것 같지만 가급적 yarn을 권장합니다. yarn이 설치되었다면 아래의 명령어를 입력하여 의존성 패키지를 설치합니다.

```sh
$ yarn
```

이 프로젝트는 두 개의 런타임을 가지고 있습니다. 하나는 스토리북이고 다른 하나는 어플리케이션 런타임입니다. 스토리북은 `yarn storybook`을 통해서 실행시킬 수 있으며, 어플리케이션은 `yarn start`를 입력하여 개발모드로 실행할 수 있습니다.

```sh
$ yarn storybook
$ yarn start
```

프로덕션 빌드로 실행하시려면 다음의 명령어를 따르시면 됩니다.

```sh
$ yarn build; yarn serve
```

## 파일 구조

프론트엔드에 관련된 소스 코드는 대부분 `/src` 아래에 위치하고 있습니다. `/src` 아래의 구조는 다음과 같습니다.

```
src
 ├──components
 │   ├──BeerCard
 │   └──...
 ├──containers
 │   ├──BeerCardContainer
 │   └──...
 ├──stores
 │   ├──AppStore
 │   └──...
 ├──constants.ts
 ├──index.html
 ├──index.tsx
 ├──injectGlobal.ts
 ├──storybook-utils.ts
 ├──types.ts
 └──utils.ts
```

- `components`: Presentational Component가 들어있는 디렉토리입니다.
- `containers`: Container Component가 들어있는 디렉토리입니다.
- `stores`: MobX, MobX State Tree기반의 스토어 및 모델이 들어있는 디렉토리입니다.
- `constants.ts`: 앱에서 사용되는 글로벌 상수가 정의된 파일입니다.
- `index.html`: Parcel 번들러의 엔트리 포인트로 사용되는 기본 HTML 파일입니다.
- `index.tsx`: React 렌더의 시작점입니다.
- `injectGlobal.ts`: styled-components 기반의 글로벌 스타일을 적용하는 파일입니다.
- `storybook-utils.tsx`: Storybook에서만 활용되는 유틸리티가 모여있는 파일입니다.
- `types.ts`: 공통적인 타입이 모여있는 파일입니다.
- `utils.ts`: 공통으로 사용하는 유틸리티가 모여있는 파일입니다.

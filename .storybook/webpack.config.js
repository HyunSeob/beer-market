const path = require('path')

module.exports = (baseConfig, env, defaultConfig) => {
  defaultConfig.module.rules = [
    ...defaultConfig.module.rules,
    {
      test: /\.(ts|tsx)$/,
      use: [{
        loader: require.resolve('ts-loader'),
        options: {
          configFile: path.resolve(__dirname, './tsconfig.json'),
        },
      }, ],
    },
  ]

  defaultConfig.resolve.extensions = ['.tsx', '.ts', '.js', '.png', '.jpg']
  defaultConfig.resolve.modules = [
    ...defaultConfig.resolve.modules,
    '../node_modules',
  ]

  return defaultConfig
}
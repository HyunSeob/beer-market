const path = require('path')
const express = require('express')
const serveStatic = require('serve-static')
const bodyParser = require('body-parser')
const Bundler = require('parcel-bundler')
const beersJson = require('../data/beers.json')
const tagsJson = require('../data/tags.json')

const isDevelopment = process.env.NODE_ENV === 'development'

const app = express()

app.use(bodyParser.json())
app.use(serveStatic(path.join(__dirname, '../public')));

app.get('/api/beers', (req, res) => {
  res.json(beersJson)
})

app.get('/api/tags', (req, res) => {
  res.json(tagsJson)
})

app.post('/api/purchase', (req, res) => {
  if (Math.random() < 0.5) {
    return res.status(500).send({
      title: '운이 안좋아 터진 에러',
      reason: 'Math.random()을 돌렸더니 0.5보다 적은 값이 나왔습니다. 다시 시도해보면 이번엔 될지도 몰라요.',
    })
  }

  const purchaseList = req.body

  const result = purchaseList.reduce((ret, purchaseItem) => {
    const beer = beersJson.find(v => v.id === purchaseItem.id)

    return {
      totalCount: ret.totalCount + purchaseItem.count,
      totalPrice: ret.totalPrice + beer.price * purchaseItem.count,
    }
  }, {
    totalCount: 0,
    totalPrice: 0,
  })

  res.json(result)
})

if (isDevelopment) {
  const bundler = new Bundler(path.join(__dirname, '../src/index.html'))

  app.use('/', bundler.middleware())
} else {
  app.use('/', express.static(path.join(__dirname, '../dist'), {
    maxAge: '7d'
  }))
  app.use('*', (req, res) => {
    res.sendFile(path.join(__dirname, '../dist/index.html'))
  })
}

const server = app.listen(1234, () => {
  const host = server.address().address;
  const port = server.address().port;
  console.info('Server listening at http://%s:%s in %s mode', host, port, process.env.NODE_ENV);
})
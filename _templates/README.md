# 이게 뭔가요?

React에서 컴포넌트를 만들때 필요한 파일들의 템플릿입니다. 컴포넌트 파일을 쉽게 생성해주는 도구인 [Hygen](http://www.hygen.io/)과 함께 사용합니다.

# 어떻게 사용하나요?

먼저 Hygen을 설치하고,

``` sh
npm install -g hygen
```

다음과 같이 사용합니다.

## 1. styled-component인 경우

``` sh
hygen component styled --name [컴포넌트이름]
```

## 2. functional-component인 경우

``` sh
hygen component functional --name [컴포넌트이름]
```

## 3. container인 경우

``` sh
hygen component container --name [컨테이너이름]
```

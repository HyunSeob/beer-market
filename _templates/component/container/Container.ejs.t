---
to: src/containers/<%= name %>/<%= name %>.tsx
---
import React, { Component } from 'react'

interface <%= name %>Props {}

class <%= name %> extends Component<<%= name %>Props> {
  render() {
    return (
      <div>
        <%= name %>
      </div>
    )
  }
}

export default <%= name %>

---
to: src/components/<%= name %>/<%= name %>.tsx
---
import React, { PureComponent } from 'react'

interface <%= name %>Props {}

class <%= name %> extends PureComponent<<%= name %>Props> {
  render() {
    return (
      <div><%= name %></div>
    )
  }
}

export default <%= name %>

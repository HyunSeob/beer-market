---
to: src/components/<%= name %>/<%= name %>.tsx
---
import styled from 'styled-components'

const <%= name %> = styled.div`
  width: 100%;
`

export default <%= name %>

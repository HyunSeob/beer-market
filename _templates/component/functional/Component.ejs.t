---
to: src/components/<%= name %>/<%= name %>.tsx
---
import React from 'react'

interface <%= name %>Props {}

const <%= name %>: React.SFC<<%= name %>Props> = () => (
  <div>
    <%= name %>
  </div>
)

export default <%= name %>

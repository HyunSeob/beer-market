import React from 'react'
import { Provider } from 'mobx-react'
import { RenderFunction } from '@storybook/react'
import AppStore from './stores/AppStore'
import { MemoryRouter } from 'react-router'

export function injectStore(story: RenderFunction) {
  return <Provider appStore={AppStore.create()}>{story()}</Provider>
}

export function injectRouter(story: RenderFunction) {
  return <MemoryRouter>{story()}</MemoryRouter>
}

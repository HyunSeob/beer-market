import { lighten, darken, desaturate } from 'polished'

// Colors
export const PRIMARY_BLUE = '#3369e8'
export const PRIMARY_BLUE_ACTIVE = lighten(0.1, PRIMARY_BLUE)
export const PRIMARY_BLUE_DISABLED = desaturate(0.5, PRIMARY_BLUE)
export const GRAY = 'rgba(58, 73, 97, 0.07)'
export const GRAY_ACTIVE = 'rgba(58, 73, 97, 0.03)'
export const WEAK_BLUE = '#c9d5f3'
export const WHITE = '#ffffff'
export const WHITE_ACTIVE = darken(0.1, WHITE)

// Size
export const NAVIGATION_HEIGHT = 52

import styled, { css } from 'styled-components'
import {
  PRIMARY_BLUE,
  GRAY,
  GRAY_ACTIVE,
  PRIMARY_BLUE_ACTIVE,
  WHITE,
} from '../../constants'

interface ButtonProps {
  palette?: 'primary' | 'secondary'
}

const Button = styled.button.attrs<ButtonProps>({
  type: 'button',
})`
  border-radius: 4px;
  padding: 8px 12px;
  font-size: 12px;
  font-weight: 300;
  background-color: ${PRIMARY_BLUE};
  border: 1px solid ${PRIMARY_BLUE};
  color: ${WHITE};
  text-decoration: none;
  transition: color 0.25s ease, background-color 0.25s ease, border-color 0.25s ease;

  &:active {
    background-color: ${PRIMARY_BLUE_ACTIVE};
    border-color: ${PRIMARY_BLUE_ACTIVE};
  }

  &:disabled {
    background-color: #dcdce0;
    border-color: #dcdce0;
    color: rgba(0, 0, 0, 0.38);
  }

  ${(p: ButtonProps) =>
    p.palette === 'secondary' &&
    css`
      background-color: ${GRAY};
      border-color: ${GRAY};
      color: #3a4961;

      &:active {
        background-color: ${GRAY_ACTIVE};
        border-color: ${GRAY_ACTIVE};
      }
    `};
`

export default Button

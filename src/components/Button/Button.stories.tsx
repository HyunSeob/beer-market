import React from 'react'
import { storiesOf } from '@storybook/react'
import Button from './Button'

storiesOf('Button', module)
  .add('default / primary', () => <Button>담기</Button>)
  .add('secondary', () => <Button palette="secondary">빼기</Button>)

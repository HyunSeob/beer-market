import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import Tag from './Tag'
import styled from 'styled-components'
import tagsJson from '../../../data/tags.json'

const Wrapper = styled.div`
  & > * {
    margin-right: 6px;
  }
`

storiesOf('Tag', module).add('default', () => (
  <Wrapper>
    {tagsJson.map(v => (
      <Tag tagKey={v.key} onChange={action('On Change')}>
        {v.name}
      </Tag>
    ))}
  </Wrapper>
))

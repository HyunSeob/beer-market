import React, { PureComponent, ChangeEvent } from 'react'
import styled from 'styled-components'
import { hideVisually } from 'polished'
import autobind from 'autobind-decorator'
import { PRIMARY_BLUE, WHITE } from '../../constants'

const Wrapper = styled.div`
  display: inline-block;
`

const Label = styled.label`
  display: inline-block;
  border: 1px solid #c9d5f3;
  color: #4e7bfa;
  background-color: transparent;
  border-radius: 4px;
  padding: 8px 12px;
  font-size: 12px;
  cursor: pointer;
  transition: color 0.25s ease, background-color 0.25s ease, border-color 0.25s ease;
`

const Checkbox = styled.input.attrs({
  type: 'checkbox',
})`
  ${hideVisually()};

  &:checked {
    + ${Label} {
      background-color: ${PRIMARY_BLUE};
      border: 1px solid ${PRIMARY_BLUE};
      color: ${WHITE};
    }
  }
`

interface TagProps {
  tagKey: string
  onChange: (key: string, checked: boolean) => void
}

class Tag extends PureComponent<TagProps> {
  static defaultProps = {
    onChange: () => {},
  }

  get id() {
    return '__checkbox_' + this.props.tagKey
  }

  @autobind
  handleChange(event: ChangeEvent<HTMLInputElement>) {
    this.props.onChange(this.props.tagKey, event.target.checked)
  }

  render() {
    const { children } = this.props

    return (
      <Wrapper>
        <Checkbox id={this.id} onChange={this.handleChange} />
        <Label htmlFor={this.id}>{children}</Label>
      </Wrapper>
    )
  }
}

export default Tag

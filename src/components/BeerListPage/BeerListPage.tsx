import { observer } from 'mobx-react'
import React, { Component } from 'react'
import styled from 'styled-components'
import BeerCardContainer from '../../containers/BeerCardContainer'
import TagListContainer from '../../containers/TagListContainer'
import { IBeerModel } from '../../stores/BeerModel'
import Navigation from '../Navigation'
import PageTemplate from '../PageTemplate'
import { WHITE_ACTIVE, WHITE } from '../../constants'

const BeerList = styled.div`
  padding: 0 11px 10px 11px;

  & > div:not(:last-child) {
    margin-bottom: 10px;
  }
`

const ButtonWrapper = styled.div`
  display: flex;
  justify-content: center;
  padding: 14px 0;
`

const LoadMoreButton = styled.button.attrs({
  type: 'button',
})`
  padding: 12px 16px 10px 16px;
  background-color: ${WHITE};
  border: 0;
  border-radius: 18px;
  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.07), 0 2px 4px 0 rgba(0, 0, 0, 0.02),
    0 6px 20px 0 rgba(0, 0, 0, 0.04);
  font-weight: bold;
  font-size: 15px;
  color: #95959e;
  transition: background-color 0.25s ease;

  &:active {
    background-color: ${WHITE_ACTIVE};
  }
`

interface BeerListPageProps {
  beers: IBeerModel[]
  beerCountInCart: number
  isAllLoaded: boolean
  onClickLoadMore: () => void
}

@observer
class BeerListPage extends Component<BeerListPageProps> {
  static defaultProps = {
    onClickLoadMore: () => {},
  }

  render() {
    const { beers, beerCountInCart, isAllLoaded, onClickLoadMore } = this.props

    return (
      <PageTemplate>
        <Navigation beerCount={beerCountInCart} />
        <TagListContainer />
        <BeerList>
          {beers.map(beer => (
            <BeerCardContainer key={beer.id} beer={beer} />
          ))}
        </BeerList>
        {!isAllLoaded && (
          <ButtonWrapper>
            <LoadMoreButton onClick={onClickLoadMore}>더보기 +</LoadMoreButton>
          </ButtonWrapper>
        )}
      </PageTemplate>
    )
  }
}

export default BeerListPage

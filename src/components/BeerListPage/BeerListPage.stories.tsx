import React from 'react'
import { storiesOf } from '@storybook/react'
import BeerListPage from './BeerListPage'
import { injectStore, injectRouter } from '../../storybook-utils'
import beersJson from '../../../data/beers.json'
import AppStore from '../../stores/AppStore'
import { action } from '@storybook/addon-actions'
import { boolean, withKnobs } from '@storybook/addon-knobs'

const { beerStore } = AppStore.create()
beerStore.addBeers(beersJson)

storiesOf('BeerListPage', module)
  .addDecorator(withKnobs)
  .addDecorator(injectStore)
  .addDecorator(injectRouter)
  .add('default', () => (
    <BeerListPage
      beers={beerStore.beerList}
      beerCountInCart={beerStore.beerCountInCart}
      isAllLoaded={!boolean('더 보기', true)}
      onClickLoadMore={action('On Load More')}
    />
  ))

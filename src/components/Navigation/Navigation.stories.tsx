import { number, withKnobs } from '@storybook/addon-knobs'
import { storiesOf } from '@storybook/react'
import React from 'react'
import { injectRouter } from '../../storybook-utils'
import Navigation from './Navigation'

storiesOf('Navigation', module)
  .addDecorator(withKnobs)
  .addDecorator(injectRouter)
  .add('default', () => <Navigation beerCount={number('맥주 숫자', 0)} />)

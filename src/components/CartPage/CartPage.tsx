import { observer } from 'mobx-react'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import BeerCardContainer from '../../containers/BeerCardContainer'
import { IBeerModel } from '../../stores/BeerModel'
import Button from '../Button'
import Navigation from '../Navigation'
import PageTemplate from '../PageTemplate'
import { numberFormatter } from '../../utils'
import { PRIMARY_BLUE_DISABLED, WHITE } from '../../constants'

const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-top: 70px;
`

const Heading = styled.h2`
  font-size: 20px;
  color: #6e6e78;
  font-weight: 500;
  margin: 21px 0 11px 0;
`

const Paragraph = styled.p`
  font-size: 14px;
  color: #95959e;
  text-align: center;
  margin: 0 0 17px 0;
`

const ExtendedButton = Button.extend`
  padding-left: 50px;
  padding-right: 50px;
`

const Purchase = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  padding: 20px 0;
`

const Entry = styled.span`
  display: block;
  font-size: 20px;
  color: #95959e;
`

const EntryNumber = styled.span`
  font-weight: 500;
  color: #768399;
`

const BigButton = Button.extend`
  width: 100%;
  font-size: 20px;
  padding: 13px 0;

  &:disabled {
    border-color: ${PRIMARY_BLUE_DISABLED};
    background-color: ${PRIMARY_BLUE_DISABLED};
    color: ${WHITE};
  }
`

const StyledLink = ExtendedButton.withComponent(Link)

const Cart = styled.div`
  padding: 16px 11px;

  & > div:not(:last-child) {
    margin-bottom: 10px;
  }
`

const BagIcon = () => (
  <svg
    width="60px"
    height="56px"
    viewBox="0 0 60 56"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g
        transform="translate(-130.000000, -122.000000)"
        fill="#3C3C42"
        fillRule="nonzero"
      >
        <g transform="translate(130.000000, 122.000000)">
          <path d="M59,14 L59,0 L1,0 L1,14 L0,14 L0,51.259 C0,53.873 2.127,56 4.742,56 L55.259,56 C57.873,56 60,53.873 60,51.259 L60,14 L59,14 Z M56.5,14 L51.32,7.094 L56.414,2 L57,2 L57,14 L56.5,14 Z M11,14 L11,7 C11,6.976 10.988,6.954 10.986,6.93 C10.981,6.866 10.966,6.806 10.95,6.743 C10.939,6.701 10.94,6.658 10.923,6.618 C10.914,6.596 10.896,6.579 10.886,6.557 C10.859,6.502 10.821,6.455 10.783,6.405 C10.755,6.369 10.739,6.324 10.706,6.292 L6.414,2 L53.586,2 L49.294,6.292 C49.262,6.324 49.245,6.369 49.217,6.405 C49.179,6.455 49.142,6.502 49.115,6.557 C49.104,6.579 49.087,6.595 49.077,6.618 C49.06,6.658 49.061,6.702 49.05,6.743 C49.033,6.806 49.018,6.865 49.014,6.93 C49.012,6.954 49,6.976 49,7 L49,14 L11,14 Z M6,14 L9,9.999 L9,14 L6,14 Z M51,9.999 L54,14 L51,14 L51,9.999 Z M3,2 L3.586,2 L8.68,7.094 L3.5,14 L3,14 L3,2 Z M58,51.259 C58,52.771 56.77,54 55.258,54 L4.742,54 C3.23,54 2,52.771 2,51.259 L2,16 L58,16 L58,51.259 Z" />
          <path d="M42,22 C41.448,22 41,22.447 41,23 L41,29 C41,35.065 36.065,40 30,40 C23.935,40 19,35.065 19,29 L19,23 C19,22.447 18.552,22 18,22 C17.448,22 17,22.447 17,23 L17,29 C17,36.168 22.832,42 30,42 C37.168,42 43,36.168 43,29 L43,23 C43,22.447 42.552,22 42,22 Z" />
          <path d="M20,23 C20,23.553 20.448,24 21,24 C21.552,24 22,23.553 22,23 C22,20.794 20.206,19 18,19 C15.794,19 14,20.794 14,23 C14,23.553 14.448,24 15,24 C15.552,24 16,23.553 16,23 C16,21.897 16.897,21 18,21 C19.103,21 20,21.897 20,23 Z" />
          <path d="M42,19 C39.794,19 38,20.794 38,23 C38,23.553 38.448,24 39,24 C39.552,24 40,23.553 40,23 C40,21.897 40.897,21 42,21 C43.103,21 44,21.897 44,23 C44,23.553 44.448,24 45,24 C45.552,24 46,23.553 46,23 C46,20.794 44.206,19 42,19 Z" />
        </g>
      </g>
    </g>
  </svg>
)

interface CartPageProps {
  beers: IBeerModel[]
  beerCountInCart: number
  priceInCart: number
  submitting: boolean
  onClickPurchase: () => void
}

@observer
class CartPage extends Component<CartPageProps> {
  static defaultProps = {
    onClickPurchase: () => {},
  }

  renderBody() {
    const {
      beerCountInCart,
      priceInCart,
      beers,
      submitting,
      onClickPurchase,
    } = this.props

    if (beerCountInCart === 0) {
      return (
        <Content>
          <BagIcon />
          <Heading>카트가 비었습니다.</Heading>
          <Paragraph>
            목록에서 원하는 맥주를
            <br />
            카트에 담아보세요.
          </Paragraph>
          <StyledLink to="/">목록으로 가기</StyledLink>
        </Content>
      )
    }

    return (
      <Cart>
        {beers.map(beer => (
          <BeerCardContainer key={beer.id} beer={beer} isInCart={true} />
        ))}
        <Purchase>
          <Entry style={{ marginBottom: '4px' }}>
            총 구매수량 <EntryNumber>{beerCountInCart}</EntryNumber>개
          </Entry>
          <Entry style={{ marginBottom: '16px' }}>
            총 결제금액 <EntryNumber>{numberFormatter.format(priceInCart)}</EntryNumber>원
          </Entry>
          <BigButton onClick={onClickPurchase} disabled={submitting}>
            {submitting ? '구매중..' : '구매하기'}
          </BigButton>
        </Purchase>
      </Cart>
    )
  }

  render() {
    const { beerCountInCart } = this.props

    return (
      <PageTemplate>
        <Navigation beerCount={beerCountInCart} />
        {this.renderBody()}
      </PageTemplate>
    )
  }
}

export default CartPage

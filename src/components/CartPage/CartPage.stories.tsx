import { action } from '@storybook/addon-actions'
import { number, withKnobs, boolean } from '@storybook/addon-knobs'
import { storiesOf } from '@storybook/react'
import React from 'react'
import beersJson from '../../../data/beers.json'
import AppStore from '../../stores/AppStore'
import { injectRouter } from '../../storybook-utils'
import CartPage from './CartPage'

const { beerStore } = AppStore.create()
beerStore.addBeers(beersJson)
beerStore.beerList.forEach(beer => {
  beer.addToCart()
})

storiesOf('CartPage', module)
  .addDecorator(withKnobs)
  .addDecorator(injectRouter)
  .add('default', () => (
    <CartPage
      beers={beerStore.beerList}
      beerCountInCart={number('맥주 갯수', 0)}
      priceInCart={number('결제 금액', 10000)}
      submitting={boolean('구매중', false)}
      onClickPurchase={action('On Purchase')}
    />
  ))

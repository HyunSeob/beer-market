import styled from 'styled-components'
import { NAVIGATION_HEIGHT } from '../../constants'

const PageTemplate = styled.div`
  min-height: 100vh;
  background-color: #f2f3f7;
  padding-top: ${NAVIGATION_HEIGHT}px;
`

export default PageTemplate

import React, { Component } from 'react'
import styled from 'styled-components'
import { observer } from 'mobx-react'
import Button from '../Button'
import { IBeerModel } from '../../stores/BeerModel'
import { numberFormatter } from '../../utils'
import { WHITE } from '../../constants'

const Wrapper = styled.div`
  background: ${WHITE};
  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.07), 0 6px 20px 0 rgba(0, 0, 0, 0.04),
    0 0 0 1px rgba(0, 0, 0, 0.02);
  border-radius: 4px;
  padding: 12px;
`

const Content = styled.div`
  display: flex;
`

const ContentLeft = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 56px;
  height: 81px;
  margin-right: 12px;
  border: 1px solid #ebebed;
`

const Image = styled.img`
  width: 100%;
  max-height: 100%;
`

const Name = styled.h2`
  font-size: 18px;
  color: #3c3c42;
  margin: 0;
  font-weight: 300;
`

const Tags = styled.span`
  font-size: 12px;
  font-weight: 500;
  color: #95959e;
`

const Price = styled.span`
  font-weight: 500;
  font-size: 16px;
  color: #768399;
`

const Currency = styled.span`
  font-size: 12px;
  color: #3c3c42;
`

const Entry = styled.span`
  margin-right: 4px;
`

const EntryLabel = styled.span`
  font-weight: 300;
  font-size: 14px;
  color: #6e6e78;
`

const EntryNumber = styled.span`
  font-size: 14px;
  font-weight: 400;
  color: #3c3c42;
`

const ButtonGroup = styled.div`
  display: flex;
  justify-content: flex-end;
  padding-top: 6px;

  & > ${Button}:not(:last-child) {
    margin-right: 6px;
  }
`

interface BeerCardProps {
  beer: IBeerModel
  isInCart: boolean
  onClickRemove: () => void
  onClickAdd: () => void
}

@observer
class BeerCard extends Component<BeerCardProps> {
  static defaultProps = {
    onClickRemove: () => {},
    onClickAdd: () => {},
  }

  render() {
    const { beer, isInCart, onClickRemove, onClickAdd } = this.props

    return (
      <Wrapper>
        <Content>
          <ContentLeft>
            <Image src={beer.image} alt={beer.name} />
          </ContentLeft>
          <div>
            <Name>{beer.name}</Name>
            <div style={{ marginBottom: '4px' }}>
              <Tags>{beer.tags.map(v => v.name).join(', ')}</Tags>
            </div>
            <div style={{ marginBottom: '4px' }}>
              <Price>{numberFormatter.format(beer.price)}</Price>
              <Currency>원</Currency>
            </div>
            <div>
              {!isInCart && (
                <Entry>
                  <EntryLabel>재고 </EntryLabel>
                  <EntryNumber>{beer.currentStock}</EntryNumber>
                </Entry>
              )}
              {beer.countInCart > 0 && (
                <Entry>
                  <EntryLabel>수량 </EntryLabel>
                  <EntryNumber>{beer.countInCart}</EntryNumber>
                </Entry>
              )}
            </div>
          </div>
        </Content>
        <ButtonGroup>
          {beer.countInCart > 0 && (
            <Button type="button" palette="secondary" onClick={onClickRemove}>
              {isInCart ? '취소' : '빼기'}
            </Button>
          )}
          {!isInCart && (
            <Button type="button" disabled={beer.currentStock === 0} onClick={onClickAdd}>
              담기
            </Button>
          )}
        </ButtonGroup>
      </Wrapper>
    )
  }
}

export default BeerCard

import { action } from '@storybook/addon-actions'
import { boolean, withKnobs } from '@storybook/addon-knobs'
import { storiesOf } from '@storybook/react'
import React from 'react'
import beersJson from '../../../data/beers.json'
import AppStore from '../../stores/AppStore'
import BeerCard from './BeerCard'

const { beerStore } = AppStore.create()
beerStore.addBeers(beersJson)
const beer = beerStore.beerList[0]
beer.addToCart()

storiesOf('BeerCard', module)
  .addDecorator(withKnobs)
  .add('default', () => (
    <BeerCard
      beer={beer}
      isInCart={boolean('Is in Cart', false)}
      onClickRemove={action('On Remove')}
      onClickAdd={action('On Add')}
    />
  ))

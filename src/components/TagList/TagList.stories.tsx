import React from 'react'
import { storiesOf } from '@storybook/react'
import TagList from './TagList'
import TagStore from '../../stores/TagStore'
import tagsJson from '../../../data/tags.json'

const tagStore = TagStore.create()
tagStore.addTags(tagsJson)

storiesOf('TagList', module).add('default', () => <TagList tags={tagStore.tagList} />)

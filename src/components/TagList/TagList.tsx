import React, { Component } from 'react'
import styled from 'styled-components'
import { observer } from 'mobx-react'
import Tag from '../Tag'
import { ITagModel } from '../../stores/TagModel'

const List = styled.ul`
  display: flex;
  list-style: none;
  margin: 0;
  padding: 12px 18px;
  width: 100%;
  overflow-x: auto;
  white-space: nowrap;

  & > li {
    margin-right: 6px;
  }
`

interface TagListProps {
  tags: ITagModel[]
  onChangeTag: (key: string, checked: boolean) => void
}

@observer
class TagList extends Component<TagListProps> {
  static defaultProps = {
    onChangeTag: () => {},
  }

  render() {
    const { tags, onChangeTag } = this.props

    return (
      <List>
        {tags.map(tag => (
          <li key={tag.key}>
            <Tag tagKey={tag.key} onChange={onChangeTag}>
              {tag.name}
            </Tag>
          </li>
        ))}
      </List>
    )
  }
}

export default TagList

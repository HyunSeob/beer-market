import { injectGlobal } from 'styled-components'
import { normalize } from 'polished'

injectGlobal`
  ${normalize()}

  * {
    box-sizing: border-box;
  }

  button, html [type="button"], [type="reset"], [type="submit"] {
    -webkit-appearance: none;
  }

  html, body {
    font-family: AppleSDGothicNeo, sans-serif;
    font-weight: 300;
  }
`

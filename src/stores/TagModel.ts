import { types, SnapshotIn } from 'mobx-state-tree'

const TagModel = types
  .model('TagModel', {
    key: types.identifier,
    name: types.string,
  })
  .volatile(self => ({
    selected: false,
  }))
  .actions(self => {
    function setSelected(selected: boolean) {
      self.selected = selected
    }

    return { setSelected }
  })

type TagModelType = typeof TagModel.Type
export interface ITagModel extends TagModelType {}
export interface ITagModelSnapshotIn extends SnapshotIn<typeof TagModel> {}

export default TagModel

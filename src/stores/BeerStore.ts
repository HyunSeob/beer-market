import axios from 'axios'
import { types, getRoot, flow } from 'mobx-state-tree'
import compose from 'lodash/fp/flow'
import map from 'lodash/fp/map'
import filter from 'lodash/fp/filter'
import sortBy from 'lodash/fp/sortBy'
import reduce from 'lodash/fp/reduce'
import take from 'lodash/fp/take'
import BeerModel, { IBeerModelSnapshotIn, IBeerModel } from './BeerModel'
import { IAppStore } from './AppStore'

const PAGING_SIZE = 5

const BeerStore = types
  .model('BeerStore', {
    beers: types.map(BeerModel),
  })
  .volatile(() => ({
    page: 1,
  }))
  .views(self => {
    function getAppStore() {
      return getRoot<IAppStore>(self)
    }

    function getAllBeerList() {
      return Array.from(self.beers.values())
    }

    return {
      get appStore() {
        return getAppStore()
      },
      get beerList() {
        return getAllBeerList()
      },
      get sortedBeerList() {
        interface ExtendedBeerItem {
          beer: IBeerModel
          intersection: number
        }

        const {
          tagStore: { selectedTagList },
        } = getAppStore()
        const beers = getAllBeerList()

        if (selectedTagList.length === 0) {
          return take(self.page * PAGING_SIZE)(getAllBeerList())
        }

        return compose(
          map<IBeerModel, ExtendedBeerItem>(v => ({
            beer: v,
            intersection: v.getIntersection(selectedTagList),
          })),
          sortBy<ExtendedBeerItem>(v => v.intersection * -1),
          map<ExtendedBeerItem, IBeerModel>(v => v.beer),
          take(self.page * PAGING_SIZE),
        )(beers)
      },
      get beerCountInCart() {
        return getAllBeerList().reduce((ret, beer) => ret + beer.countInCart, 0)
      },
      get beerListInCart() {
        return getAllBeerList().filter(v => v.isInCart)
      },
      get priceInCart() {
        return compose(
          filter<IBeerModel>(v => v.isInCart),
          reduce<IBeerModel, number>((ret, beer) => ret + beer.priceInCart, 0),
        )(getAllBeerList())
      },
      get isAllLoaded() {
        return self.beers.size <= self.page * PAGING_SIZE
      },
    }
  })
  .actions(self => {
    const fetchBeers = flow(function*() {
      const { data } = yield axios.get('/api/beers')
      addBeers(data)
    })

    const requestPurchase = flow<{ totalPrice: number; totalCount: number }>(function*() {
      const body = self.beerListInCart.map(({ id, countInCart }) => ({
        id,
        count: countInCart,
      }))

      const { data } = yield axios.post('/api/purchase', body)
      return data
    })

    function addBeers(beers: IBeerModelSnapshotIn[]) {
      beers.forEach(beer => {
        const { tags, ...rest } = beer
        self.appStore.tagStore.addTags(tags)
        self.beers.put({ ...rest, tags: tags.map(v => v.key) })
      })
    }

    function clearCart() {
      self.beers.forEach(beer => {
        beer.clearFromCart()
      })
    }

    function loadMoreBeers() {
      self.page++
    }

    return { fetchBeers, requestPurchase, addBeers, clearCart, loadMoreBeers }
  })

export default BeerStore

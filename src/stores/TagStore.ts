import axios from 'axios'
import { types, flow } from 'mobx-state-tree'
import TagModel, { ITagModelSnapshotIn } from './TagModel'

const TagStore = types
  .model('TagStore', {
    tags: types.map(TagModel),
  })
  .views(self => {
    function getTagList() {
      return Array.from(self.tags.values())
    }

    return {
      get tagList() {
        return getTagList()
      },
      get selectedTagList() {
        return getTagList().filter(v => v.selected)
      },
    }
  })
  .actions(self => {
    const fetchTags = flow(function*() {
      const { data } = yield axios.get('/api/tags')
      addTags(data)
    })

    function addTags(tags: ITagModelSnapshotIn[]) {
      tags.forEach(tag => {
        self.tags.put(tag)
      })
    }

    return { fetchTags, addTags }
  })

export default TagStore

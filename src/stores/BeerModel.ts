import { types, SnapshotIn } from 'mobx-state-tree'
import intersection from 'lodash/intersection'
import TagModel, { ITagModelSnapshotIn, ITagModel } from './TagModel'
import { Omit } from '../types'

const BeerModel = types
  .model('BeerModel', {
    id: types.identifierNumber,
    name: types.string,
    image: types.string,
    price: types.number,
    stock: types.number,
    tags: types.array(types.reference(TagModel)),
  })
  .volatile(self => {
    return {
      countInCart: 0,
    }
  })
  .views(self => {
    return {
      get currentStock() {
        return Math.max(self.stock - self.countInCart, 0)
      },
      get isInCart() {
        return self.countInCart > 0
      },
      get priceInCart() {
        return self.price * self.countInCart
      },
      getIntersection(tags: ITagModel[]) {
        return intersection(self.tags, tags).length
      },
    }
  })
  .actions(self => {
    function addToCart() {
      self.countInCart++
    }

    function removeFromCart() {
      self.countInCart--
    }

    function clearFromCart() {
      self.countInCart = 0
    }

    return {
      addToCart,
      removeFromCart,
      clearFromCart,
    }
  })

type BeerModelType = typeof BeerModel.Type
export interface IBeerModel extends BeerModelType {}
export interface IBeerModelSnapshotIn extends Omit<SnapshotIn<typeof BeerModel>, 'tags'> {
  tags: ITagModelSnapshotIn[]
}

export default BeerModel

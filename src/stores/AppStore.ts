import { types } from 'mobx-state-tree'
import BeerStore from './BeerStore'
import TagStore from './TagStore'

const AppStore = types.model('AppStore', {
  beerStore: types.optional(BeerStore, BeerStore.create()),
  tagStore: types.optional(TagStore, TagStore.create()),
})

type AppStoreType = typeof AppStore.Type
export interface IAppStore extends AppStoreType {}

export default AppStore

import { AxiosError } from 'axios'

export const numberFormatter = new Intl.NumberFormat('ko-KR', {
  currency: 'KRW',
  currencyDisplay: 'name',
})

export function isAxiosError(error: Error): error is AxiosError {
  return !!(error as AxiosError).response
}

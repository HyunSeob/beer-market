import { Provider } from 'mobx-react'
import React from 'react'
import ReactDOM from 'react-dom'
import { Route, Switch } from 'react-router'
import { BrowserRouter } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'
import 'setimmediate'
import 'react-toastify/dist/ReactToastify.css'
import BeerListPageContainer from './containers/BeerListPageContainer'
import CartPageContainer from './containers/CartPageContainer'
import './injectGlobal'
import AppStore from './stores/AppStore'

ReactDOM.render(
  <Provider appStore={AppStore.create()}>
    <BrowserRouter>
      <div>
        <Switch>
          <Route exact={true} path="/" component={BeerListPageContainer} />
          <Route exact={true} path="/cart" component={CartPageContainer} />
        </Switch>
        <ToastContainer />
      </div>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root'),
)

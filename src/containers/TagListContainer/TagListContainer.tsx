import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'
import autobind from 'autobind-decorator'
import { IAppStore } from '../../stores/AppStore'
import TagList from '../../components/TagList'

interface TagListContainerProps {}

interface InjectedProps extends TagListContainerProps {
  appStore: IAppStore
}

@inject('appStore')
@observer
class TagListContainer extends Component<TagListContainerProps> {
  get injected() {
    return this.props as InjectedProps
  }

  componentDidMount() {
    this.injected.appStore.tagStore.fetchTags()
  }

  @autobind
  handleChangeTag(key: string, checked: boolean) {
    const { tagStore } = this.injected.appStore
    const tag = tagStore.tags.get(key)

    if (tag) {
      tag.setSelected(checked)
    }
  }

  render() {
    const { tagStore } = this.injected.appStore
    return <TagList tags={tagStore.tagList} onChangeTag={this.handleChangeTag} />
  }
}

export default TagListContainer

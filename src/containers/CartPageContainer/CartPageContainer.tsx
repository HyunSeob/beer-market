import { inject, observer } from 'mobx-react'
import React, { Component } from 'react'
import CartPage from '../../components/CartPage'
import { IAppStore } from '../../stores/AppStore'
import autobind from 'autobind-decorator'
import { toast } from 'react-toastify'
import { isAxiosError } from '../../utils'
import { observable, action } from 'mobx'

interface CartPageContainerProps {}

interface InjectedProps extends CartPageContainerProps {
  appStore: IAppStore
}

@inject('appStore')
@observer
class CartPageContainer extends Component<CartPageContainerProps> {
  @observable
  submitting = false

  get injected() {
    return this.props as InjectedProps
  }

  @action
  setSubmitting(submitting: boolean) {
    this.submitting = submitting
  }

  @autobind
  async handleClickPurchase() {
    if (this.submitting) {
      return
    }

    this.setSubmitting(true)

    const { beerStore } = this.injected.appStore

    try {
      const { totalPrice, totalCount } = await beerStore.requestPurchase()

      toast.success(
        <div>
          구매가 완료되었습니다.
          <ul>
            <li>총 결제금액: {totalPrice}</li>
            <li>총 구매수량: {totalCount}</li>
          </ul>
        </div>,
        {
          autoClose: false,
        },
      )

      beerStore.clearCart()
    } catch (err) {
      if (isAxiosError(err) && err.response) {
        toast.error(err.response.data.reason, {
          autoClose: false,
        })
      }

      throw err
    } finally {
      this.setSubmitting(false)
    }
  }

  render() {
    const { beerStore } = this.injected.appStore

    return (
      <CartPage
        beers={beerStore.beerListInCart}
        beerCountInCart={beerStore.beerCountInCart}
        priceInCart={beerStore.priceInCart}
        submitting={this.submitting}
        onClickPurchase={this.handleClickPurchase}
      />
    )
  }
}

export default CartPageContainer

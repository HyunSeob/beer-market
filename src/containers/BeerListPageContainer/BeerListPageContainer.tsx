import autobind from 'autobind-decorator'
import { inject, observer } from 'mobx-react'
import React, { Component } from 'react'
import BeerListPage from '../../components/BeerListPage'
import { IAppStore } from '../../stores/AppStore'

interface BeerListPageContainerProps {}

interface InjectedProps extends BeerListPageContainerProps {
  appStore: IAppStore
}

@inject('appStore')
@observer
class BeerListPageContainer extends Component<BeerListPageContainerProps> {
  get injected() {
    return this.props as InjectedProps
  }

  componentDidMount() {
    this.injected.appStore.beerStore.fetchBeers()
  }

  @autobind
  handleClickLoadMore() {
    this.injected.appStore.beerStore.loadMoreBeers()
  }

  render() {
    const { beerStore } = this.injected.appStore

    return (
      <BeerListPage
        beers={beerStore.sortedBeerList}
        beerCountInCart={beerStore.beerCountInCart}
        isAllLoaded={beerStore.isAllLoaded}
        onClickLoadMore={this.handleClickLoadMore}
      />
    )
  }
}

export default BeerListPageContainer

import { inject, observer } from 'mobx-react'
import React, { Component } from 'react'
import Navigation from '../../components/Navigation'
import { IAppStore } from '../../stores/AppStore'

interface NavigationContainerProps {}

interface InjectedProps extends NavigationContainerProps {
  appStore: IAppStore
}

@inject('appStore')
@observer
class NavigationContainer extends Component<NavigationContainerProps> {
  get injected() {
    return this.props as InjectedProps
  }

  render() {
    const { beerStore } = this.injected.appStore
    return <Navigation beerCount={beerStore.beerCountInCart} />
  }
}

export default NavigationContainer

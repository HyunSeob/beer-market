import React, { Component } from 'react'
import autobind from 'autobind-decorator'
import { IBeerModel } from '../../stores/BeerModel'
import BeerCard from '../../components/BeerCard'

interface BeerCardContainerProps {
  beer: IBeerModel
  isInCart: boolean
}

class BeerCardContainer extends Component<BeerCardContainerProps> {
  static defaultProps = {
    isInCart: false,
  }

  @autobind
  handleClickAdd() {
    this.props.beer.addToCart()
  }

  @autobind
  handleClickRemove() {
    this.props.beer.removeFromCart()
  }

  render() {
    const { beer, isInCart } = this.props

    return (
      <BeerCard
        beer={beer}
        isInCart={isInCart}
        onClickAdd={this.handleClickAdd}
        onClickRemove={this.handleClickRemove}
      />
    )
  }
}

export default BeerCardContainer
